

(defparameter *variable* "This is a pre set variable")
(defparameter *CHROMOSOME_SIZE* 12)
(defparameter *POPULATION_SIZE* 10)

(defun main () 

	;output by default
	(format t "~& Mary had a little lamb.")
	
	;calling a funtion
	(line-break)
	(format t *variable*)
	(line-break)
	
	;setting a varible via function
	(setf number 1234)
	(format t "~A" number) ; ~A parses to string
	(line-break)
	(setf number (setNumber))
	(format t "~A" number)
	
	
	
	
	
)


(defun init ()



)



(defun line-break ()

	(format t "~%")
)


(defun setNumber()
	
	(setf dif 20000)

)


; "(testSorting)" to run
(defun testSorting()	
	
	;set up array
	;(setf starting-chrom (make-array (list *POPULATION_SIZE* *CHROMOSOME_SIZE*) :initial-element 0))
	
	(setf SORT_THIS (make-array '(5 3) :initial-contents '((1 2 3)
															(12 15 8)
															(20 1 -5)
															(5 6 7)
															( 8 12 0))))
	
	
	;Sort array via another function
	(setf SORT_THIS (sort-by-fitness SORT_THIS))
	(write-2D-array SORT_THIS)
)


(defun sort-by-fitness (population)

	(setf loopNum (nth 0(array-dimensions population))) ; sets loop to 5
	(format t "~A" loopNum)
	(line-break)
	(setf loopNum (- loopNum 1)) ; subtacts 1 from loop
	(format t "~A" loopNum)
	(line-break)
	
	
	(dotimes (i loopNum)
		(format t "~&Congragulations")
		(dotimes (j loopNum)
		
			(if ( > (aref population j (- 3 1)) (aref population (+ j 1) (- 3 1)))
				(setf population (swap-values population j))
			)
			
		)
	)
	
	(return-from sort-by-fitness population)
	
)


(defun swap-values (population index)

	;store value in temp
	(setf temp (make-array (list 3)))

	;(setf temp (aref population index nth 0)) - DOESN'T WORK
	
	;manual copy because i can't figure it out
	(dotimes (k 3)
			
		(setf (aref temp k) (aref population index k))
		(setf (aref population index k) (aref population (+ index 1) k))
		(setf (aref population (+ index 1) k) (aref temp k))
	)
	
	(return-from swap-values population)
	
	;overwrite index value
	;(setf (aref population index) (aref population (+ 1 index)))
	
	;MANUAL OVERWRITE
	
	
	;place temp at index + 1
	;(setf (aref population (+ 1 index)) temp)
)


(defun write-2D-array (input)

	(dotimes (m (nth 0(array-dimensions input)))
	
		(format t "( ")
		
		(dotimes (n (nth 1(array-dimensions input)))
		
			(setf temp(aref input m n))
			(format t "~A" temp)
			(format t ", ")
		
		)
		
		(format t ")")
		(line-break)
	)

)


(defun testRounding ()

	(setf randoms (get-randoms 3 1000000))
	(format t "~&Randoms : ")
	(WriteArray randoms)
	(line-break)

	(setf position (make-array '(3)))
	
	(dotimes (i 3)
		
		(setf (aref position i) (* (aref randoms i) 231960))
	
	)
	
	(format t "~&Before rounding: ")
	(WriteArray position)
	(line-break)
	
	(dotimes ( i 3)
	
		(setf (aref position i) (round (aref position i )))
	)
	
	(format t "~&After rounding: ")
	(WriteArray position)
	(line-break)


)


;Pass in a size and decimal, returns an array of random numbers between 0 and 1. 
;eg decimal 100 = 0.25, decimal 1000 = 0.257, decimal 10000 = 0.2579 etc....
(defun get-randoms (amount decimal)

	(setf output (make-array (list amount)))

	(dotimes (i amount)
	
		;make random number
		(setf randy (random decimal)) ; makes random between 0 and 100
		(setf randy ( / randy decimal)) ; random now between 0 and 1
	
		;assign random number to output
		(setf (aref output i) randy)
		
	)
	
	;return output
	(return-from get-randoms output)

)

(defun WriteArray (input)

	(format t "( ")

	(dotimes ( y (nth 0 (array-dimensions input)))
	
		(format t "~A" (aref input y))
		(format t ", ")
	
	)
	
	(format t ")")
	(line-break)	

)



