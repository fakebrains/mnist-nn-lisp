FAKE BRAINS > Patrick Collins & Danny Lynch
DKIT Heuristics CA1
GD4
March 2018

**README**
V0.0

TO-DO
1 Research
--1A. Take a look at simple NN implementations in LISP - maybe starting with single perceptron models and moving
	to multiple layer models
--1B. Take a look at existing MNIST implementations in LISP
2 What needs to happen
--2A. MNIST data read out to usable images. Use original MNIST or translated library of some kind
--2B. Once broken down into N images, read pixel data of images, get numerical value, send as input to input layer.
----	i. Input layer is 784 neurons (so NN format needs bigtime scalability). Optionally, don't feed all of image. Try half (top, bottom, left, right, or middle square)
--2C. How many layers do we need? I've seen a diagram of this done with 2 layers, 784 input neurons and 10 output neurons...seems cumbersome. Thinking 3 layers.
----	i. 784 input, 10 output, seems ideal to put a middle hidden layer of some factor between 784 and 10 (784/14 = 56 - this might be a good N to step down to, or 28 also works with 0 modulus)
3 Implementation
--3A. Having done the research, figured out how to read MNIST data or alternative and pass into NN, and decided on depth and breadth of NN, get to work coding in LISP
4 Train and Test
--4A. Train with MNIST, Test with MNIST, compute accuracy
5 Tweak!
--5A  Start with 2 layers (784 10) see what the results are, how many epochs to train (if at all), then try 3 layers (784 N 10) where N is 56 or 28
6  Submit
--5A. Wrap up -> Complete documentation, print documentation, email and present


But Wait! There's More!

We're going to use MNIST data that's been parsed out to CSV, read in from CSV, put values into list and use list as inputs. Initial value is
the number pictured (0-9) which we can use for error checking.

This is hairy.

FIRST: ASDF - Another System Definition Facility - this is a package handling facility for lisp - think MAKE FILES
Info: https://common-lisp.net/project/asdf-install/tutorial/index-save.html
Raw Code: https://common-lisp.net/project/asdf/archives/asdf.lisp

Copy raw code into a text, rename to asdf.lisp. Put this in root dir of clisp or root dir of c:/users/<your username>
Follow instruction on INFO above to add asdf init details to clisprc (make sure to use the DIR you used above)

YOU THOUGHT YOU WERE DONE!!! :-)

Get Quicklisp - think NODE for LISP - it's handy!
INFO: https://www.quicklisp.org/beta/
This site has the lisp file and detailed instructions. Recommend loading, installing, and selecting the option to auto-load quicklisp when you start lisp. Makes life easy.

IF you've successfully installed and added to lisp init ASDF and Quicklisp, restart clisp and punch in: (ql:quickload :cl-csv) ... it should load and you're now ready to parse CSV files in LISP. Neural Network, here we come!!!

07 March --UPDATE

Added in nn.lsp - this is the Tanimoto backprop neural network. We need to change the inputs to our csv read-in, change the layers to 2 (to start), and set the height in neurons for each layer. No work has commenced on this yet, as of 16:39. At this point, don't know if we can do a NN with GA weights. One idea, sum outputs if result is > 1 (meaning NN returns more than a single 1 value), run GA weights. Second check is if sum of outputs =1, then run backprop on weights? Maybe the other way around??

NOW THAT YOU HAVE QUICKLISP - there are some libraries available for machine learning and neural network. I started messing with them but had no luck, but I only tried a couple. Not sure if going this route is better than adapting the Tanimoto NN...but could be worth investigating further.


