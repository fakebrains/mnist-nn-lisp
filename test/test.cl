
(cl-csv:read-csv #P"./test3.csv")
(format nil "~{~{~a,  ~}~% ~}" (cl-csv:read-csv #P"./test3.csv"))

(cl-csv:read-csv #P"test3.csv"
                 :map-fn #'(lambda (row)
                             (make-instance 'object
                                            :foo (nth 0 row)
                                            :baz (nth 2 row))))
											
											
(let ((data (cl-csv:read-csv #P"test3.csv" :separator #\;)))
  (values (apply #'vector (first data))
          (apply #'vector (rest (mapcar #'first data)))
          (apply #'vector 
             (mapcar #'read-from-string (loop :for row :in (rest data)
                                              :append (rest row))))))
											  
											  
											  
(cl-csv:read-csv-row #P"./test.csv" 5) &key (separator *separator*) (quote *quote*) (escape *quote-escape*) &aux current state line llen c elen) => *result*
[Function] read-csv-row ( stream-or-string &key (separator *separator*) (quote *quote*) (escape *quote-escape*) &aux current state line llen c elen) => *result*

(cl-csv:read-csv-row "test3.csv" (5 row))

(iter (for row in-csv #P"test3.csv") 
      (for i from 0) 
      (when (= i 5)
        (return row)))
		
		(cl-csv::with-csv-input-stream (str #P"test3.csv") 
     (let ( row )
       (dotimes (i 5 row) 
          (setf row (cl-csv:read-csv-row str :h))))
		  )
		  
		  (cl-csv::with-csv-input-stream (str #P"test3.csv")
     (let ( row )
       (dotimes (i 5 row)
          (setf row (cl-csv:read-csv-row str)))) :h)