; Genetic Algorythem Neural Network to solve MINST

; Neural Net will have 3 Hidden Layers, an input and output layer
; 1 - input - 784 inputs - 1 for each pixle, 0 means empty, 1 is full
; 2 - Hidden_1 - 785 inputs (including dummy), 265 outputs
; 3 - Hidden_2 - 266 inputs (including Dummy), 88 outputs
; 4 - Hidden_3 - 89 inputs (including Dummy), 10 outputs
; 5 - Output - 10 outputs

; Chromosome contains 231,960 values - Dummies not included (Dummies don't need to be in our Chromosome since they're not a variable)
; 1 - First 207,760 for weights of Hidden_1 (784 x 265) 	= 207,760
; 2 - Next 23,320 values for Hidden_2 (265 x 88)			+ 23,320 = 231,080
; 3 - Final 880 values for Hidden_3 (88 x 10)				+ 880 = 231,960

; ---------------- THIS IS HOW WE DO IT ----------------

;0 - generate our initial cromosone
;1 - read our chromosome to make our A1, A2 and A3. (the weights of each neuron)
;2 - read in our next input from CVS
;3 - read in our desired output from CVS
;4 - Add dummy 1 to initial inputs
;4.1 - Add dummy weights to A1
;5 - Matrix multiply inputs and weights
;6 - Apply heavyside to outputs 
; ---------------- LAYER 2 ----------------
;7 - Add dummy 1 to our outputs (now our inputs for layer 2)
;8 - Add dummy weight to A2
;9 - Matrix multiply inputs by weights
;10 - Apply heavyside to outputs
; ---------------- LAYER 3 ----------------
;11 - Add dummy 2 to our outputs (becomes inputs for layer 3)
;12 - Add dummy weight to A3
;13 - Matrix Multiply inputs and outputs 
;14 - Apply Heavy side to outputs
;15 - Subtract Desired output from output to get errors
;16 - Modify fitness based on errors
;17 - Repeat 2 - 16 until all pictures are done
;18 - Get next chromosome in our population
;19 - Repeat 1 - 18 until all chromosome in population are tested

; ---------------- EVOLVE OUR POPULATION ----------------
;20 - Sort population by fitness
;21 - Bump off 2 least fitness
;22 - Encode each chromosome - Using value encoding so chromosomes are already encoded
;23 - Decide 10 sets of parents (roulette wheel)
;24 - Crossover next 2 parents to make child
;25 - Mutate child
;26 - Decode child
;27 - Repeat 24 - 26 until all parents have mated
;28 - Repeat 1 - 28 until we have a fit population
;29 - Kick off again

(defparameter *CHROMOSOME_SIZE* 231961) ; 1 extra to contain fitness
(defparameter *POPULATION_SIZE* 10) ; Up to 70 works
(defparameter *N_PARENTS* 8)			; Number of parents that get to mate
(defparameter *N_MUTATIONS* 3)			; Number of values to mutate
(defparameter *N_ELITE* 2)				; Number of elite to get a free ride
(defparameter *number_dataset* 60000)
(defparameter *n_layer_1_inputs* 784) 	;number of layer 1 inputs
(defparameter *n_layer_2_inputs* 265)
(defparameter *n_layer_3_inputs* 88)
(defparameter *n_outputs* 10)

(defun init ()
	;make 2D array with 231,961 elements all set to 0 (one extra element to contain the fitness)
	(setf starting-pop (make-array (list *POPULATION_SIZE* *CHROMOSOME_SIZE*) :initial-element 1))

	;make A1 - zero filled - 207760 elements total
	(setf A1 (make-array (list *n_layer_1_inputs* *n_layer_2_inputs*) :initial-element 0)) ;- A1 has 784 inputs and 265 outputs
	(setf A2 (make-array (list *n_layer_2_inputs* *n_layer_3_inputs*) :initial-element 0))
	(setf A3 (make-array (list *n_layer_3_inputs* *n_outputs*) :initial-element 0))
	(setf Output (make-array (list *n_outputs*) :initial-element 0))
		
	(setf count 0)  ; holds the iterator for the chromosome
	(setf popIndex 0) ; Holds the iterator for our population
		
		
	;REMEMBER - Each weigh matrix will have:	a row for every input
	;											a column for every output
	
	;set A1 values from chromosome
	(dotimes (i *n_layer_1_inputs*) ; for every row
	
		(dotimes (j *n_layer_2_inputs*) ; for every column
		
			(setf (aref A1 i j) (aref starting-pop popIndex count))
			(incf count) ; increment count
		
		); end column - every 265 we start a new column
	
	);end row
	
	(format t "~& A1 Set - ")
	(format t "~A" count)
	(format t " total values assigned")
	(line-break)
	

	;set A2 values from chromosome
	(dotimes (i *n_layer_2_inputs*) ; for every row
	
		(dotimes (j *n_layer_3_inputs*) ; for every column
		
			(setf (aref A2 i j) (aref starting-pop popIndex count))
			(incf count) ; increment count
		
		); end column - every 88 we start a new column
	
	);end row
	
	(format t "~& A2 Set - ")
	(format t "~A" count)
	(format t " total values assigned")
	(line-break)
	
	;set A3 values from chromosome
	(dotimes (i *n_layer_3_inputs*) ; for every row
	
		(dotimes (j *n_outputs*) ; for every column
		
			(setf (aref A3 i j) (aref starting-pop popIndex count))
			(incf count) ; increment count
		
		); end column - every 10 we start a new column
	
	);end row

	(format t "~& A3 Set - ")
	(format t "~A" count)
	(format t " total values assigned")
	(line-break)
	
	(format t "~& Initialization Completed")
	
	;get MINST dataset
	;(defparameter *inputs*			
	;	(make-array (list *number_dataset*) 
	;		:initial-contents (mapcar #'make-inputs '(
	;		;ADD FUNCTION TO GET EACH ROW FROM CVS AS AN ELEMENT IN A LIST 
	;		)
	;	)))
		
		
		
		
	; -------- Genetic Algorythem Begining --------
	;1- take our initial population and evolve it
	

	(dotimes ( e 10)
	
	(setf starting-pop (get-new-population starting-pop))
	(format t "~& New population generated")
	
	)
	
	
	
	
			
	
)

;For testing purposes - Writes the entire weight matrix to console
(defun WriteWeights()

	(Write2DArray A1)
	(Write2DArray A2)
	(Write2DArray A3)
	
)


;Pass in 2D array, displays it's contents to console
(defun Write2DArray (input)

	(dotimes ( a (nth 0 (array-dimensions input)))
	
		(format t "( ")
	
		(dotimes (b (nth 1 (array-dimensions input)))
		
			(format t "~A" (aref input a b))
			(format t ", ")
		)

		(format t ")")
		(line-break)
	)	


)

(defun WriteArray (input)

	(format t "( ")

	(dotimes ( y (nth 0 (array-dimensions input)))
	
		(format t "~A" (aref input y))
		(format t ", ")
	
	)
	
	(format t ")")
	(line-break)	

)



; Input 2D list of population, outputs evolved population
(defun get-new-population (old_population)

	(setf pair-num (/ *POPULATION_SIZE* 2)) ; number of parent pairs needed (half the populationsize)

	;Declare new population and parents
	(setf new-population (make-array (list *POPULATION_SIZE* *CHROMOSOME_SIZE*)))
	(setf parents (make-array (list *N_PARENTS* *CHROMOSOME_SIZE*)))	
	(setf parent_1 (make-array (list pair-num))) ; a index list of parent_1's
	(setf parent_2 (make-array (list pair-num))) ; a index list of parent_2's
	(setf child_1 (make-array (list *CHROMOSOME_SIZE*) :initial-element 1))
	(setf child_2 (make-array (list *CHROMOSOME_SIZE*) :initial-element 1))
	
	
	;Get array of random numbers for selection
	(setf randoms (get-randoms (* *POPULATION_SIZE* 2) 100))
	(setf randomIndex 0) ; holds the positional value of which random number to use next
	(format t "~& Randoms Generated")
	
	;Sort our population by fitness (fitness is the final value in the chromosome)
	(setf old_population (sort-by-fitness old_population ))
	(format t "~& Population sorted")
	;(WriteArray randoms)
	
	;Assign parents, kill off the weakest
	(dotimes ( x *N_PARENTS*)
	
		(dotimes (y *CHROMOSOME_SIZE*)
		
			(setf (aref parents x y) (aref old_population x y)) 			
		)
		
	)
	
	
	;Get total fitness of parents
	(setf total-fitness (get-total-fitness parents))
	
	
	;Add ELITE members to parent_1 list
	(dotimes ( x *N_ELITE*)
	
		(setf (aref parent_1 x) x)
	)

	
	;Define the rest of Parent_1 list
	(dotimes (x (- pair-num *N_ELITE*))
		
		;Will return an index to a suitable mate based on the random number
		(setf mate (Find-Mate parents (aref randoms randomIndex) total-fitness))
		
		
		(setf (aref parent_1 (+ x *N_ELITE*)) mate)
		(incf randomIndex)
	)
	(format t "~&Parents 1")
	(WriteArray parent_1)
	
	
	;Define Parent_2 List
	(dotimes ( x  pair-num)
	
		;Will return an index to a suitable mate based on the random number
		(setf mate (Find-Mate parents (aref randoms randomIndex) total-fitness))
		(setf (aref parent_2 x) mate)
		(incf randomIndex)
	)
	(format t "~&Parents 2")
	(WriteArray parent_2)
	

	
	;MakeChildren - Crossover, Mutation and Acceptance 
	(dotimes ( x pair-num) ; Loop for every parent pair
		
		
		(dotimes ( y (- *CHROMOSOME_SIZE* 1)) ; loop for every gene in chromosome (-1 to exclude fitness)
			
			;Handle crossover - Using blend crossover
			
			;Child 1 = Parent1 - random * (Parent1 - Parent2)
			;Child 2 = Parent2 + random * (Parent1 - Parent2)
			(setf gene1 ( - (aref parents (aref parent_1 x) y) (aref parents (aref parent_2 x) y))) ;ie (Parent1 - Parent2)	
			(setf gene1 (* gene1 (aref randoms randomIndex))) ; ie (Parent1 - Parent2) * randomNumber
			(setf gene1 (* gene1 -1))							; ie (Parent1 - Parent2) * -randomNumber
			(setf gene1 (+ gene1 (aref parents (aref parent_1 x) y))); ie ((Parent1 - Parent2) * - randomNumber) + Parent1
			(setf (aref child_1 y) gene1) ; set child1 gene value
			
			(setf gene2 ( - (aref parents (aref parent_1 x) y) (aref parents (aref parent_2 x) y))) ;ie (Parent1 - Parent2)
			(setf gene2 (* gene2 (aref randoms randomIndex))); ie (Parent1 - Parent2) * randomNumber
			(setf gene2 (+ gene2 (aref parents (aref parent_2 x) y))); ie ((Parent1 - Parent2) * randomNumber) + Parent2)
			(setf (aref child_2 y) gene2) ; set child_2 gene value
		)
		
		;Mutate children
		(setf child_1 (Mutate-Chromosome child_1))
		(setf child_2 (Mutate-Chromosome child_2))
		
		;Add fitness of 0 on end of chomosome
		(setf (aref child_1 (- *CHROMOSOME_SIZE* 1)) 1)
		(setf (aref child_2 (- *CHROMOSOME_SIZE* 1)) 1)
		
		;Add children to new popuation
		(dotimes ( z  *CHROMOSOME_SIZE* )

			;Child 1
			(setf (aref new-population x z) (aref child_1 z))
			
			;Child 2
			(setf (aref new-population (+ x (/ *POPULATION_SIZE* 2)) z) (aref child_2 z))
		
		)
	)
	
	;return new popualtion
	new-population
	
	
	
	
)

;Pass in chomosome, returnes mutated chromosome
(defun Mutate-Chromosome (chromosome)

	;Get positions of mutations
	(setf temp-randoms (get-randoms *N_MUTATIONS* 1000000))
	(setf mutate-positions (make-array (list *N_MUTATIONS*)))
	(setf mutate-quantity (make-array (list *N_MUTATIONS*)))
	
	(dotimes ( p  *N_MUTATIONS*)
		
		;Multiply random number (eg 0.123456) by chomosome size - fitness value
		(setf (aref mutate-positions p) (* (aref temp-randoms p) (- *CHROMOSOME_SIZE* 1)))
		
		;round position to nearest
		(setf (aref mutate-positions p) (round (aref mutate-positions p)))
		
		;Get quantity to mutate by (between -1.00 and 1.00)
		(setf quantity (random 200)) ; ie between 0 and 200
		(setf quantity (- quantity 100)) ; ie between -100 and 100
		(setf quantity (/ quantity 100)) ; ie between -1.00 and 1.00
		(setf (aref mutate-quantity p) quantity) ; set quantity
		
	)
	
	;perform mutation
	(dotimes ( p *N_MUTATIONS*)
	
		;increment the chromosome gene at mutation-position by mutate-quanity
		(incf (aref chromosome (aref mutate-positions p)) (aref mutate-quantity p))
	)
	
	;return chromosome
	chromosome
	
)

;Pass in parents, random number and total fitness. Finds a mate based via roulette selection
(defun Find-Mate ( pool _random total-fitness)

	(setf cumulative-fitness 0)
	(setf normalized 0)
	
	;Loop through parent pool
	(dotimes (j (nth 0 (array-dimensions pool)))
	
		(incf cumulative-fitness (aref pool j (- *CHROMOSOME_SIZE* 1)))
		(setf normalized (/ cumulative-fitness total-fitness))
		
		;if our random number is <= normalized then we have found our mate
		(if ( <= _random normalized)
			(return-from Find-Mate j))
		
	
	)
	
	;In case something goes wrong
	(format t "~& Find-Mate Error: Selected least fit member")
	(return-from Find-Mate (nth 0 (array-dimensions pool)))
) 

;Pass in 2D array of population, returns the total fitness of the population
(defun get-total-fitness (input_population)
	
	(setf result 0)
	
	;loop for every row in population
	(dotimes ( c (nth 0 (array-dimensions input_population)))
		
		;incrment result by the final value of each chromosome for the population
		(incf result (aref input_population c (- *CHROMOSOME_SIZE* 1)))
	)

	result
)


;Pass in a size and decimal, returns an array of random numbers between 0 and 1. 
;eg decimal 100 = 0.25, decimal 1000 = 0.257, decimal 10000 = 0.2579 etc....
(defun get-randoms (amount decimal)

	(setf output (make-array (list amount)))

	(dotimes (i amount)
	
		;make random number
		(setf randy (random decimal)) ; makes random between 0 and 100
		(setf randy ( / randy decimal)) ; random now between 0 and 1
	
		;assign random number to output
		(setf (aref output i) randy)
		
	)
	
	;return output
	(return-from get-randoms output)

)

; Input 2D list of population, returns same list but sorted by final value in chromosome (our fitness)
(defun sort-by-fitness (population)

	(setf loopNum (nth 0 (array-dimensions population))) ; sets loopnum to number of rows in population (ie our population number)
	(setf loopNum (- loopNum 1)) ; Subtract 1 for the bubble sort


	; for every element except the last - This is a simple bubble sort algorythem
	(dotimes( i loopNum)
		
		(dotimes(j loopNum)

			(if( > (aref population j ( - *CHROMOSOME_SIZE* 1)) (aref population (+ j 1) (- *CHROMOSOME_SIZE* 1)))
					then (setf population (swap-values population j))
					
			)
				
		)
		
	)
	
	(return-from sort-by-fitness population)

)


; Pass in a 2D List and an index to swap at (ie swaps chromosomes) - used by sort-by-fitness
(defun swap-values (population index)


	;for temp storage 
	(setf columnNum (nth 0 (array-dimensions population))) ; gets number of columns in population
	(setf temp (make-array (list columnNum)))

	(dotimes (k columnNum)
	
		(setf (aref temp k) (aref population index k))
		(setf (aref population index k) (aref population (+ index 1) k))
		(setf (aref population (+ index 1) k) (aref temp k))
	
	)
	
	(return-from swap-values population)
	
)






; Function to return an agrument in an array
(defun make-inputs (data)
	(make-array (list *n_layer_1_inputs*)
		:initial-contents data)
)


;function to multiply two matracies 
(defun multiply (a b)

	(let* 	(
				(m (nth 0 (array-dimensions a)))
				(s (nth 1 (array-dimensions a)))
				(n (nth 1 (array-dimensions b)))
				(result	(make-array (list m n )))
	
			)
	(dotimes (i m)
		(dotimes (j n)
		
			(setf (aref result i j) 0.0)
			(dotimes (k s)
				
				(incf (aref result i j) (* (aref a i k) (aref b k j)))
			
			)
		
		
		)
		
	)

	result 
)
)

; Function to add Dummy 1 to a matrix
(defun prepend1 (a)

	(let* 	(
				(m (nth 0 (array-dimensions a)))
				(n (nth 1 (array-dimensions a )))
				(result (make-array (list m (+ 1 n))))
	
			)
		
		(dotimes( i m )
			(setf (aref result i 0) 1.0)
		)
		
		(dotimes (i m)
			(dotimes (j n)
				(setf (aref result i ( + 1 j)) (aref a i j))
			)
		)
		result 
	)
)

; Function to apply heavy side to a value
(defun heavyside (x)
	(if (< x 0) 0 1)
)

; Function to apply heavyside to a matrix
(defun f (a)
	(let*	(
				(m (nth 0 (array-dimensions a)))
				(n (nth 1 (array-dimensions a)))
				(result (make-array (list m n)))	
			)
		
		(dotimes ( i m)
			(dotimes (j n)
				(setf (aref result i j) (heavyside (aref a i j )))
				
			)
		
		)
	result
	)
)

; Neural network function
; Input two matracies (input and weights) and will give output
(defun nn (x w)
	(let*	(
				(m (nth 0 (array-dimensions x)))
				(n (nth 1 (array-dimensions w )))
				(result (make-array (list m n)))
	
			)
		(setf result (f (mult (prepend1 x) x)))
	)
)


;Makes a line break
(defun line-break ()

	(format t "~%")
)